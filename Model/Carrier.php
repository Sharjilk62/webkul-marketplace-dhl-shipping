<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpDHLShipping
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpDHLShipping\Model;

use Magento\Catalog\Model\Product\Type;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Framework\Xml\Security;
use Magento\Framework\Session\SessionManager;
use Magento\Shipping\Model\Shipping\LabelGenerator;

/**
 * Marketplace DHL shipping.
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Carrier extends \Magento\Dhl\Model\Carrier
{
    /**
     * Code of the carrier.
     *
     * @var string
     */
    const CODE = 'mpdhl';
    /**
     * Code of the carrier.
     *
     * @var string
     */
    protected $_code = self::CODE;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager = null;
    /**
     * [$_coreSession description].
     *
     * @var [type]
     */
    protected $_coreSession;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    /**
     * @var [type]
     */
    protected $_region;
    /**
     * @var LabelGenerator
     */
    protected $_labelGenerator;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    /**
     * Rate result data.
     *
     * @var Result|null
     */
    protected $_result = null;

    protected $_customerFactory;

    protected $requestParam;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface             $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory     $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                       $logger
     * @param Security                                                       $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory               $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory                     $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory    $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory                 $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory           $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory          $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory                         $regionFactory
     * @param \Magento\Directory\Model\CountryFactory                        $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory                       $currencyFactory
     * @param \Magento\Directory\Helper\Data                                 $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface           $stockRegistry
     * @param \Magento\Store\Model\StoreManagerInterface                     $storeManager
     * @param \Magento\Framework\Module\Dir\Reader                           $configReader
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\ObjectManagerInterface                      $objectManager
     * @param SessionManager                                                 $coreSession
     * @param \Magento\Customer\Model\Session                                $customerSession
     * @param LabelGenerator                                                 $labelGenerator
     * @param array                                                          $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Shipping\Helper\Carrier $carrierHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $coreDate,
        \Magento\Framework\Module\Dir\Reader $configReader,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\Math\Division $mathDivision,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readFactory,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\RequestInterface $requestInterface,
        SessionManager $coreSession,
        \Magento\Customer\Model\Session $customerSession,
        LabelGenerator $labelGenerator,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Request\Http $requestParam,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->requestParam = $requestParam;
        $this->requestInterface = $requestInterface;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $carrierHelper,
            $coreDate,
            $configReader,
            $storeManager,
            $string,
            $mathDivision,
            $readFactory,
            $dateTime,
            $httpClientFactory,
            $data
        );
        $this->_objectManager = $objectManager;
        $this->_coreSession = $coreSession;
        $this->_customerSession = $customerSession;
        $this->_region = $regionFactory;
        $this->_labelGenerator = $labelGenerator;
        $this->_customerFactory = $customerFactory;
    }

    /**
     * Collect and get rates.
     *
     * @param RateRequest $request
     *
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error|bool|Result
     */
    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        if (!$this->canCollectRates()
            || $this->isMultiShippingActive()
        ) {
            return false;
        }
        $this->setRequest($request);
        $this->_result = $this->getShippingPricedetail($this->_rawRequest);

        return $this->getResult();
    }

    /**
     * isMultiShippingActive
     */
    private function isMultiShippingActive()
    {
        $routeName = $this->requestInterface->getRouteName();
        if ($routeName == 'multishipping' && $this->_scopeConfig->getValue('carriers/mpmultishipping/active')) {
            return false;
        } elseif ($this->_scopeConfig->getValue('carriers/mpmultishipping/active')) {
            return true;
        }
        return false;
    }

    protected function _getGatewayUrl()
    {
        if ($this->getConfigData('sandbox_mode')) {
            return $this->getConfigData('sandbox_gateway_url');
        } else {
            return $this->getConfigData('production_gateway_url');
        }
    }

    /**
     * Prepare and set request to this instance.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function setRequest(\Magento\Framework\DataObject $request)
    {
        $this->_request = $request;

        $r = new \Magento\Framework\DataObject();
        $mpassignproductId = 0;
        $shippingdetail = [];
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }
                $sellerId = 0;
                $mpassignproductId = $this->_getAssignProduct($item);
                $sellerId = $this->_getSellerId($mpassignproductId, $item->getProductId());
                $weight = $this->_getItemWeight($item);
                
                $originPostcode = '';
                $originCountryId = '';
                $originCity = '';
                if ($sellerId) {
                    $address = $this->_loadModel(
                        $sellerId,
                        'Magento\Customer\Model\Customer'
                    )->getDefaultShipping();

                    $addressModel = $this->_loadModel(
                        $address,
                        'Magento\Customer\Model\Address'
                    );
                    $originPostcode = $addressModel->getPostcode();
                    $originCountryId = $addressModel->getCountryId();
                    $originCity = $addressModel->getCity();
                } else {
                    $originPostcode = $this->_scopeConfig->getValue(
                        \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ZIP,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $r->getStoreId()
                    );
                    $originCountryId = $this->_scopeConfig->getValue(
                        \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_COUNTRY_ID,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $r->getStoreId()
                    );
                }
                if (count($shippingdetail) == 0) {
                    array_push(
                        $shippingdetail,
                        [
                            'seller_id' => $sellerId,
                            'origin_postcode' => $originPostcode,
                            'origin_country_id' => $originCountryId,
                            'origin_city' => $originCity,
                            'items_weight' => $weight,
                            'product_name' => $item->getName(),
                            'qty' => $item->getQty(),
                            'item_id' => $item->getId(),
                            'price' => $item->getPrice()*$item->getQty(),
                        ]
                    );
                } else {
                    $shipinfoflag = true;
                    $index = 0;
                    foreach ($shippingdetail as $itemship) {
                        if ($itemship['seller_id'] == $sellerId) {
                            $itemship['items_weight'] = $itemship['items_weight'] + $weight;
                            $itemship['product_name'] = $itemship['product_name'].','.$item->getName();
                            $itemship['item_id'] = $itemship['item_id'].','.$item->getId();
                            $itemship['qty'] = $itemship['qty'] + $item->getQty();
                            $itemship['price'] = $itemship['price'] + $item->getPrice()*$item->getQty();
                            $shippingdetail[$index] = $itemship;
                            $shipinfoflag = false;
                        }
                        ++$index;
                    }
                    if ($shipinfoflag == true) {
                        array_push(
                            $shippingdetail,
                            [
                                'seller_id' => $sellerId,
                                'origin_postcode' => $originPostcode,
                                'origin_country_id' => $originCountryId,
                                'origin_city' => $originCity,
                                'items_weight' => $weight,
                                'product_name' => $item->getName(),
                                'qty' => $item->getQty(),
                                'item_id' => $item->getId(),
                                'price' => $item->getPrice()*$item->getQty(),
                            ]
                        );
                    }
                }
            }
        }
        if ($request->getShippingDetails()) {
            $shippingdetail = $request->getShippingDetails();
        }
        $r->setShippingDetails($shippingdetail);

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }
        if ($destCountry == self::USA_COUNTRY_ID
            && ($request->getDestPostcode() == '00912' || $request->getDestRegionCode() == self::PUERTORICO_COUNTRY_ID)
        ) {
            $destCountry = self::PUERTORICO_COUNTRY_ID;
        }
        $r->setDestCountryId($destCountry);

        if ($request->getDestPostcode()) {
            $r->setDestPostal($request->getDestPostcode());
        }

        $r->setDestCity($request->getDestCity());
        $r->setOrigCity($request->getOrigCity());

        $this->setRawRequest($r);

        return $this;
    }
   

    /**
     * set the configuration values.
     *
     * @param \Magento\Framework\DataObject $request
     */
    public function setConfigData(\Magento\Framework\DataObject $request)
    {
        $r = $request;

        $r->setDhlAccessId($this->getConfigData('id'));
        $r->setDhlPassword($this->getConfigData('password'));
        $r->setDhlAccountNumber($this->getConfigData('account'));
        $r->setDhlReadyTime($this->getConfigData('ready_time'));

        return $r;
    }
    /**
     * set seller credentials if he/she has.
     *
     * @param \Magento\Framework\DataObject $request
     * @param int                           $sellerId
     *
     * @return \Magento\Framework\DataObject
     */
    protected function _isSellerHasOwnCredentials(\Magento\Framework\DataObject $request, $sellerId)
    {
        $customer = $this->_customerFactory->create()->load($sellerId);
        if (isset($customer['dhl_access_id'])) {
            $request->setDhlAccessId($customer->getDhlAccessId());
        } elseif ($this->getConfigData('id') == '' && !isset($customer['dhl_access_id'])) {
            $request->setDhlAccessId('');
        }
        if (isset($customer['dhl_account_number'])) {
            $request->setDhlAccountNumber($customer->getDhlAccountNumber());
        } elseif ($this->getConfigData('account') == '' && !isset($customer['dhl_account_number'])) {
            $request->setDhlAccountNumber('');
        }
        if (isset($customer['dhl_password'])) {
            $request->setDhlPassword($customer->getDhlPassword());
        } elseif ($this->getConfigData('password') == '' && !isset($customer['dhl_password'])) {
            $request->setDhlPassword('');
        }
        if (isset($customer['dhl_ready_time'])) {
            $request->setDhlReadyTime($customer->getDhlReadyTime());
        } elseif ($this->getConfigData('ready_time') == '' && !isset($customer['dhl_ready_time'])) {
            $request->setDhlReadyTime('');
        }

        return $request;
    }
    /**
     * Add dimension to piece
     *
     * @param \Magento\Shipping\Model\Simplexml\Element $nodePiece
     * @return void
     */
    protected function _addDimension($nodePiece)
    {
        $sizeChecker = (string)$this->getConfigData('size');

        $height = $this->_getDimension((string)$this->getConfigData('height'));
        $depth = $this->_getDimension((string)$this->getConfigData('depth'));
        $width = $this->_getDimension((string)$this->getConfigData('width'));

        if ($sizeChecker && $height && $depth && $width) {
            $nodePiece->addChild('Height', $height);
            $nodePiece->addChild('Depth', $depth);
            $nodePiece->addChild('Width', $width);
        }
    }
    /**
     * Get shipping quotes from DHL service
     *
     * @param string $request
     * @return string
     */
    protected function _getQuotesFromServer($request)
    {
        $client = $this->_httpClientFactory->create();
        $client->setUri((string)$this->_getGatewayUrl());
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setRawData(utf8_encode($request));

        return $client->request(\Zend_Http_Client::POST)->getBody();
    }
    /**
     * Makes remote request to the carrier and returns a response.
     *
     * @param string $purpose
     *
     * @return mixed
     */
    protected function _createRatesRequest($shipdetail)
    {

        $rawRequest = $this->_rawRequest;
        $rawRequest->setOrigCountryId($shipdetail['origin_country_id']);
        $this->originCountryId = $shipdetail['origin_country_id'];

        $xmlStr = '<?xml version = "1.0" encoding = "UTF-8"?>'.
            '<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" '.
            'xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" '.
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xsi:schemaLocation="http://www.dhl.com DCT-req.xsd "/>';
        $xml = $this->_xmlElFactory->create(['data' => $xmlStr]);
        $nodeGetQuote = $xml->addChild('GetQuote', '', '');
        $nodeRequest = $nodeGetQuote->addChild('Request');

        $nodeServiceHeader = $nodeRequest->addChild('ServiceHeader');
        $nodeServiceHeader->addChild('SiteID', (string) $rawRequest->getDhlAccessId());
        $nodeServiceHeader->addChild('Password', (string) $rawRequest->getDhlPassword());

        $nodeFrom = $nodeGetQuote->addChild('From');
        $nodeFrom->addChild('CountryCode', $shipdetail['origin_country_id']);
        $nodeFrom->addChild('Postalcode', $shipdetail['origin_postcode']);
        $nodeFrom->addChild('City', $shipdetail['origin_city']);

        $nodeBkgDetails = $nodeGetQuote->addChild('BkgDetails');
        $nodeBkgDetails->addChild('PaymentCountryCode', $shipdetail['origin_country_id']);
        $nodeBkgDetails->addChild(
            'Date',
            (new \DateTime())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT)
        );
        $nodeBkgDetails->addChild('ReadyTime', 'PT'.(int) (string) $rawRequest->getDhlReadyTime().'H00M');

        $nodeBkgDetails->addChild('DimensionUnit', $this->_getDimensionUnit());
        $nodeBkgDetails->addChild('WeightUnit', $this->_getItemWeightUnit());

        $nodePieces = $nodeBkgDetails->addChild('Pieces', '', '');
        $nodePiece = $nodePieces->addChild('Piece', '', '');
        $nodePiece->addChild('PieceID', 1);
        $this->_addDimension($nodePiece);
        $nodePiece->addChild('Weight', $this->_getActualWeight($shipdetail['items_weight']));
        
        $nodeBkgDetails->addChild('PaymentAccountNumber', (string) $rawRequest->getDhlAccountNumber());

        $nodeTo = $nodeGetQuote->addChild('To');
        $nodeTo->addChild('CountryCode', $rawRequest->getDestCountryId());
        $nodeTo->addChild('Postalcode', $rawRequest->getDestPostal());
        $nodeTo->addChild('City', $rawRequest->getDestCity());
        
        if ($this->isDutiable($rawRequest->getOrigCountryId(), $rawRequest->getDestCountryId())) {
            // IsDutiable flag and Dutiable node indicates that cargo is not a documentation
            $nodeBkgDetails->addChild('IsDutiable', 'Y');
            $nodeDutiable = $nodeGetQuote->addChild('Dutiable');

            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
            $nodeDutiable->addChild('DeclaredCurrency', $baseCurrencyCode);
            $nodeDutiable->addChild('DeclaredValue', sprintf('%.2F', $shipdetail['price']));
        }
        
        return $xml;
    }

    /**
     * Build RateV3 request, send it to Dhl gateway and retrieve quotes in XML format.
     *
     * @return Result
     */
    public function getShippingPricedetail(\Magento\Framework\DataObject $request)
    {
        $this->setConfigData($request);
        $this->setRawRequest($request);
        $r = $request;
        $submethod = [];
        $shippinginfo = [];
        $totalpric = [];
        $totalPriceArr = [];
        $serviceCodeToActualNameMap = [];
        $costArr = [];
        $debugData = [];
        $price = 0;
        $isStorePickup = false;
        $flag = false;
        $check = false;
        
        foreach ($r->getShippingDetails() as $shipdetail) {
            $priceArr = [];
            $this->_isSellerHasOwnCredentials($this->_rawRequest, $shipdetail['seller_id']);
            $responseBody = '';
            try {
                for ($offset = 0; $offset <= self::UNAVAILABLE_DATE_LOOK_FORWARD; ++$offset) {
                    $debugData['try-'.$offset] = [];
                    $debugPoint = &$debugData['try-'.$offset];

                    $requestXml = $this->_createRatesRequest($shipdetail);
                   
                    $date = date(self::REQUEST_DATE_FORMAT, strtotime($this->_getShipDate()." +{$offset} days"));
                    $this->_setQuotesRequestXmlDate($requestXml, $date);

                    $requestData = $requestXml->asXML();
                    
                    $debugPoint['request'] = $requestData;
                    $responseBody = $this->_getCachedQuotes($requestData);
                    $debugPoint['from_cache'] = $responseBody === null;

                    if ($debugPoint['from_cache']) {
                        $responseBody = $this->_getQuotesFromServer($requestData);
                    }
                    
                    $debugPoint['response'] = $responseBody;

                    $bodyXml = $this->_xmlElFactory->create(['data' => $responseBody]);
                    $code = $bodyXml->xpath('//GetQuoteResponse/Note/Condition/ConditionCode');
                    if (isset($code[0]) && (int) $code[0] == self::CONDITION_CODE_SERVICE_DATE_UNAVAILABLE) {
                        $debugPoint['info'] = sprintf(__('DHL service is not available at %s date'), $date);
                    } else {
                        break;
                    }

                    $this->_setCachedQuotes($requestData, $responseBody);
                }
                $this->_debug($debugData);
            } catch (\Exception $e) {
                $this->_errors[$e->getCode()] = $e->getMessage();
            }

            list($priceArr, $costArr) = $this->_parseRateResponse($responseBody);
            $price = 0;
            if (!$this->isMultiShippingActive()) {
                if (count($totalPriceArr) > 0 && is_array($priceArr)) {
                    foreach ($priceArr as $method => $price) {
                        if (array_key_exists($method, $totalPriceArr)) {
                            $check = true;
                            $totalPriceArr[$method] = $totalPriceArr[$method] + $priceArr[$method];
                        } else {
                            unset($priceArr[$method]);
                            $flag = $check == true ? false : true;
                        }
                    }
                } else {
                    $totalPriceArr = $priceArr;
                }
                if (count($priceArr) > 0) {
                    foreach ($totalPriceArr as $method => $price) {
                        if (!array_key_exists($method, $priceArr)) {
                            unset($totalPriceArr[$method]);
                        }
                    }
                } else {
                    $totalPriceArr = [];
                    $flag = true;
                }
            }
            if ($flag) {
                $debugData['result'] = ['error' => 1];
                if ($this->isMultiShippingActive() ||
                    $this->_scopeConfig->getValue('carriers/wk_pickup/active')) {
                    return [];
                } else {
                    return $this->_parseXmlResponse($debugData);
                }
            }
            
            /*Calculate price itemwise for seller store pickup*/
            $items = explode(',', $shipdetail['item_id']);
            $newShipderails = [];
            $newShipderails['seller_id'] = $shipdetail['seller_id'];
            $newShipderails['origin_country_id'] = $shipdetail['origin_country_id'];
            $newShipderails['origin_postcode'] = $shipdetail['origin_postcode'];
            $newShipderails['origin_city'] = $shipdetail['origin_city'];
            $itemPriceDetails = [];
            if (isset($shipdetail['item_weight_details']) &&
                isset($shipdetail['item_product_price_details']) &&
                isset($shipdetail['item_qty_details'])
                ) {
                $isStorePickup = true;
                foreach ($items as $itemId) {
                    $newShipderails['items_weight'] = $shipdetail['item_weight_details'][$itemId];
                    $newShipderails['price'] = $shipdetail['item_product_price_details'][$itemId] * $shipdetail['item_qty_details'][$itemId];
                    $requestData =  $this->_createRatesRequest($newShipderails);
                    $requestData = $requestXml->asXML();
                    $responseBody = $this->_getQuotesFromServer($requestData);
                    list($itemPriceArr, $itemCostArr) = $this->_parseRateResponse($responseBody);
                    $itemPriceDetails[$itemId] = $itemPriceArr;
                }
            }
            $submethod = [];
            foreach ($priceArr as $index => $price) {
                $submethod[$index] = [
                    'method' => $this->getDhlProductTitle($index).' (DHL)',
                    'cost' => $price,
                    'base_amount' => $price,
                    'error' => 0,
                ];
            }
            if (!isset($shipdetail['item_id_details'])) {
                $shipdetail['item_id_details'] = [];
            }
            if (!isset($shipdetail['item_name_details'])) {
                $shipdetail['item_name_details'] = [];
            }
            if (!isset($shipdetail['item_qty_details'])) {
                $shipdetail['item_qty_details'] = [];
            }
            array_push(
                $shippinginfo,
                [
                    'seller_id' => $shipdetail['seller_id'],
                    'methodcode' => $this->_code,
                    'shipping_ammount' => $price,
                    'product_name' => $shipdetail['product_name'],
                    'submethod' => $submethod,
                    'item_ids' => $shipdetail['item_id'],
                    'item_price_details' => $itemPriceDetails,
                    'item_id_details' => $shipdetail['item_id_details'],
                    'item_name_details' => $shipdetail['item_name_details'],
                    'item_qty_details' => $shipdetail['item_qty_details']
                ]
            );
        }
        $totalpric = ['totalprice' => $totalPriceArr, 'costarr' => $costArr];
        $debugData['result'] = $totalpric;
        $result = ['handlingfee' => $totalpric, 'shippinginfo' => $shippinginfo, 'error' => $debugData];
        $shippingAll = $this->_coreSession->getData('shippinginfo');
        $shippingAll = $this->_coreSession->getShippingInfo();
        $shippingAll[$this->_code] = $result['shippinginfo'];
        $this->_coreSession->setShippingInfo($shippingAll);

        if ($this->_scopeConfig->getValue('carriers/mpmultishipping/active') || $isStorePickup) {
            return $result;
        } else {
            return $this->_parseXmlResponse($totalpric);
        }
    }

    /**
     * Get the rates from response.
     * @param  xml $response
     * @return array [$priceArr, $costArr];
     */
    public function _parseRateResponse($response)
    {
        $priceArr = [];
        $costArr = [];
        if (strlen(trim($response)) > 0) {
            if (strpos(trim($response), '<?xml') === 0) {
                $xml = $this->parseXml($response, 'Magento\Shipping\Model\Simplexml\Element');
                if (is_object($xml)) {
                    if (isset($xml->GetQuoteResponse->BkgDetails->QtdShp)) {
                        foreach ($xml->GetQuoteResponse->BkgDetails->QtdShp as $quotedShipment) {
                            $dhlProduct = (string) $quotedShipment->GlobalProductCode;
                            $totalEstimate = (float) (string) $quotedShipment->ShippingCharge;
                            $currencyCode = (string) $quotedShipment->CurrencyCode;
                            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
                            $dhlProductDescription = $this->getDhlProductTitle($dhlProduct);
                            $serviceCodeToActualNameMap[$dhlProduct] = $dhlProductDescription;
                            if ($currencyCode != $baseCurrencyCode) {
                                /* @var $currency \Magento\Directory\Model\Currency */
                                $currency = $this->_currencyFactory->create();
                                $rates = $currency->getCurrencyRates($currencyCode, [$baseCurrencyCode]);
                                if (!empty($rates) && isset($rates[$baseCurrencyCode])) {
                                    // Convert to store display currency using store exchange rate
                                    $totalEstimate = $totalEstimate * $rates[$baseCurrencyCode];
                                } else {
                                    $rates = $currency->getCurrencyRates($baseCurrencyCode, [$currencyCode]);
                                    if (!empty($rates) && isset($rates[$currencyCode])) {
                                        $totalEstimate = $totalEstimate / $rates[$currencyCode];
                                    }
                                    if (!isset($rates[$currencyCode]) || !$totalEstimate) {
                                        $totalEstimate = false;
                                        $this->_errors[] = __(
                                            'We had to skip DHL method %1 because we couldn\'t 
                                            find exchange rate %2 (Base Currency).',
                                            $currencyCode,
                                            $baseCurrencyCode
                                        );
                                    }
                                }
                            }
                            if (array_key_exists(
                                (string) $quotedShipment->GlobalProductCode,
                                $this->getAllowedMethods()
                            )
                            ) {
                                if ($totalEstimate) {
                                    $costArr[$dhlProduct] = $totalEstimate;
                                    $priceArr[$dhlProduct] = $this->getMethodPrice($totalEstimate, $dhlProduct);
                                }
                            }
                        }
                        asort($priceArr);

                        return [$priceArr, $costArr];
                    } else {
                        return [$priceArr, $costArr];
                    }
                }
            }
        }
    }
    /**
     * Parse calculated rates.
     *
     * @param string $response
     *
     * @return Result
     */
    protected function _parseXmlResponse($response)
    {
        $result = $this->_rateFactory->create();
        if (isset($response['result']['error'])) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier('mpdhl');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        } else {
            $totalPriceArr = $response['totalprice'];
            $costArr = $response['costarr'];
            foreach ($totalPriceArr as $method => $price) {
                $rate = $this->_rateMethodFactory->create();
                $rate->setCarrier('mpdhl');
                $rate->setCarrierTitle($this->getConfigData('title'));
                $rate->setMethod($method);
                $rate->setMethodTitle($this->getDhlProductTitle($method));
                $rate->setCost($costArr[$method]);
                $rate->setPrice($price);
                $result->append($rate);
            }
        }

        return $result;
    }
    /**
     * Prepare shipping label data.
     *
     * @param \SimpleXMLElement $xml
     *
     * @return \Magento\Framework\DataObject
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareShippingLabelContent(\SimpleXMLElement $xml)
    {
        $result = new \Magento\Framework\DataObject();
        try {
            if (!isset($xml->AirwayBillNumber) || !isset($xml->LabelImage->OutputImage)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to retrieve shipping label'));
            }
            $result->setTrackingNumber((string) $xml->AirwayBillNumber);
            $labelContent[] = base64_decode((string) $xml->LabelImage->OutputImage);

            $outputPdf = $this->_labelGenerator->combineLabelsPdf($labelContent);
            $result->setShippingLabelContent($outputPdf->render());
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }

        return $result;
    }
    /**
     * Do request to shipment
     *
     * @param \Magento\Shipping\Model\Shipment\Request $request
     * @return array|\Magento\Framework\DataObject
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function requestToShipment($request)
    {
        $packages = $request->getPackages();
        $this->setRawRequest($request);
        $orderId = $this->requestParam->getParam('order_id');
        $request->setOrderId($orderId);

        if (!is_array($packages) || !$packages) {
            throw new \Magento\Framework\Exception\LocalizedException(__('No packages for request'));
        }
        $result = $this->_doShipmentRequest($request);

        $response = new \Magento\Framework\DataObject(
            [
                'info' => [
                    [
                        'tracking_number' => $result->getTrackingNumber(),
                        'label_content' => $result->getShippingLabelContent(),
                    ],
                ],
            ]
        );

        $request->setMasterTrackingId($result->getTrackingNumber());

        return $response;
    }

    /**
     * Do shipment request to carrier web service,.
     *
     * @param \Magento\Framework\DataObject $request
     *
     * @return \Magento\Framework\DataObject
     */
    public function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        $this->setRawRequest($request);
        $orderId = $request->getOrderId();
        $customerId = $this->_customerSession->getCustomerId();
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);
        $this->_order = $order;
        if ($this->_isShippingMethod()) {
            $this->setShipemntRequest();
            $response = $this->_createShipmentRequest();
            $result = new \Magento\Framework\DataObject();
            if (strlen(trim($response)) > 0) {
                if (strpos(trim($response), '<?xml') === 0) {
                    $xml = $this->parseXml($response, 'Magento\Shipping\Model\Simplexml\Element');
                    if (is_object($xml)) {
                        if (in_array($xml->getName(), ['ErrorResponse', 'ShipmentValidateErrorResponse'])
                        || isset($xml->GetQuoteResponse->Note->Condition)
                        ) {
                            $code = null;
                            $data = null;
                            if (isset($xml->Response->Status->Condition)) {
                                $nodeCondition = $xml->Response->Status->Condition;
                            }
                            foreach ($nodeCondition as $condition) {
                                $code = isset($condition->ConditionCode) ? (string) $condition->ConditionCode : 0;
                                $data = isset($condition->ConditionData) ? (string) $condition->ConditionData : '';
                                if (!empty($code) && !empty($data)) {
                                    break;
                                }
                            }
                            throw new \Magento\Framework\Exception\LocalizedException(
                                __('Error #%1 : %2', trim($code), trim($data))
                            );
                        } elseif (isset($xml->AirwayBillNumber)) {
                            $labelResponse = $this->_prepareShippingLabelContent($xml);
                            $result->setShippingLabelContent($labelResponse->getShippingLabelContent());
                            $result->setTrackingNumber($labelResponse->getTrackingNumber());

                            $sellerOrders = $this->_objectManager->create(
                                'Webkul\Marketplace\Model\Orders'
                            )->getCollection()
                            ->addFieldToFilter('seller_id', ['eq' => $customerId])
                            ->addFieldToFilter('order_id', ['eq' => $orderId]);

                            $shipmentData = [
                                'api_name' => 'DHL',
                                'tracking_number' => $labelResponse->getTrackingNumber()
                            ];
                            $this->_customerSession->setData('shipment_data', $shipmentData);

                            foreach ($sellerOrders as $row) {
                                $row->setShipmentLabel($labelResponse->getShippingLabelContent());
                                $row->save();
                            }
                        }
                    }
                }
            }
            return $result;
        } else {
            return false;
        }
    }
    /**
     * Make DHL Shipment Request
     * @return string xml
     */
    protected function _createShipmentRequest()
    {
        $order = $this->_order;
        $customerId = $this->_customerSession->getCustomerId();

        $request = $request = $this->_rawRequest->getShipmentDetails();

        $this->_rawRequest->setOrigCountryId($request->getShipperAddressCountryCode());

        $originRegionCode = $this->getCountryParams(
            $request->getShipperAddressCountryCode()
        )->getRegion();

        if (!$originRegionCode) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Wrong Region'));
        }

        if ($originRegionCode == 'AM') {
            $originRegionCode = '';
        }

        $xmlStr = '<?xml version="1.0" encoding="UTF-8"?>'.
            '<req:ShipmentValidateRequest'.
            $originRegionCode.
            ' xmlns:req="http://www.dhl.com"'.
            ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'.
            ' xsi:schemaLocation="http://www.dhl.com ship-val-req'.
            ($originRegionCode ? '_'.
                $originRegionCode : '').
            '.xsd" />';
        $xml = $this->_xmlElFactory->create(['data' => $xmlStr]);

        $nodeRequest = $xml->addChild('Request', '', '');
        $nodeServiceHeader = $nodeRequest->addChild('ServiceHeader');
        $nodeServiceHeader->addChild('SiteID', (string) $request->getAccessId());
        $nodeServiceHeader->addChild('Password', (string) $request->getPassword());

        if (!$originRegionCode) {
            $xml->addChild('RequestedPickupTime', 'N', '');
        }
        $xml->addChild('NewShipper', 'N', '');
        $xml->addChild('LanguageCode', 'EN', '');
        $xml->addChild('PiecesEnabled', 'Y', '');

        /* Billing */
        $nodeBilling = $xml->addChild('Billing', '', '');
        $nodeBilling->addChild('ShipperAccountNumber', (string) $request->getAccountNumber());
        /*
         * Method of Payment:
         * S (Shipper)
         * R (Receiver)
         * T (Third Party)
         */
        $nodeBilling->addChild('ShippingPaymentType', 'S');

        /*
         * Shipment bill to account – required if Shipping PaymentType is other than 'S'
         */
        $nodeBilling->addChild('BillingAccountNumber', (string) $request->getAccountNumber());
        $nodeBilling->addChild('DutyPaymentType', 'S');
        $nodeBilling->addChild('DutyAccountNumber', (string) $request->getAccountNumber());

        /* Receiver */
        $nodeConsignee = $xml->addChild('Consignee', '', '');

        $companyName = $request->getRecipientContactCompanyName() ? $request
            ->getRecipientContactCompanyName() : $request
            ->getRecipientContactPersonFullName();

        $nodeConsignee->addChild('CompanyName', substr($companyName, 0, 35));

        $address = $request->getRecipientAddressStreet1().' '.$request->getRecipientAddressStreet2();
        $address = $this->string->split($address, 35, false, true);
        if (is_array($address)) {
            foreach ($address as $addressLine) {
                $nodeConsignee->addChild('AddressLine', $addressLine);
            }
        } else {
            $nodeConsignee->addChild('AddressLine', $address);
        }

        $nodeConsignee->addChild('City', $request->getRecipientAddressCity());
        $nodeConsignee->addChild('Division', $request->getRecipientAddressStateOrProvinceCode());
        $nodeConsignee->addChild('PostalCode', $request->getRecipientAddressPostalCode());
        $nodeConsignee->addChild('CountryCode', $request->getRecipientAddressCountryCode());
        $nodeConsignee->addChild(
            'CountryName',
            $this->getCountryParams($request->getRecipientAddressCountryCode())->getName()
        );
        $nodeContact = $nodeConsignee->addChild('Contact');
        $nodeContact->addChild('PersonName', substr($request->getRecipientContactPersonFullName(), 0, 34));
        $nodeContact->addChild('PhoneNumber', substr($request->getRecipientContactPhoneNumber(), 0, 24));

        /*
         * Commodity
         * The CommodityCode element contains commodity code for shipment contents. Its
         * value should lie in between 1 to 9999.This field is mandatory.
         */
        $nodeCommodity = $xml->addChild('Commodity', '', '');
        $nodeCommodity->addChild('CommodityCode', '1');

        /* Dutiable */
        if ($this->isDutiable(
            $request->getShipperAddressCountryCode(),
            $request->getRecipientAddressCountryCode()
        )) {
            $nodeDutiable = $xml->addChild('Dutiable', '', '');
            $nodeDutiable->addChild(
                'DeclaredValue',
                sprintf('%.2F', $request->getTotalValue())
            );
            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
            $nodeDutiable->addChild('DeclaredCurrency', $baseCurrencyCode);
        }

        /*
         * Reference
         * This element identifies the reference information. It is an optional field in the
         * shipment validation request. Only the first reference will be taken currently.
         */
        $nodeReference = $xml->addChild('Reference', '', '');
        $nodeReference->addChild('ReferenceID', $order->getIncrementId());
        $nodeReference->addChild('ReferenceType', 'St');

        /* Shipment Details */
         $this->_setItemsDetails($xml, $this->_rawRequest, $originRegionCode);

        /* Shipper */
        $nodeShipper = $xml->addChild('Shipper', '', '');
        $nodeShipper->addChild('ShipperID', (string) $request->getAccountNumber());
        $nodeShipper->addChild('CompanyName', $request->getShipperContactCompanyName());
        $nodeShipper->addChild('RegisteredAccount', (string) $request->getAccountNumber());

        $address = $request->getShipperAddressStreet1().' '.$request->getShipperAddressStreet2();
        $address = $this->string->split($address, 35, false, true);
        if (is_array($address)) {
            foreach ($address as $addressLine) {
                $nodeShipper->addChild('AddressLine', $addressLine);
            }
        } else {
            $nodeShipper->addChild('AddressLine', $address);
        }

        $nodeShipper->addChild('City', $request->getShipperAddressCity());
        $nodeShipper->addChild('Division', $request->getShipperAddressStateOrProvinceCode());
        $nodeShipper->addChild('PostalCode', $request->getShipperAddressPostalCode());
        $nodeShipper->addChild('CountryCode', $request->getShipperAddressCountryCode());
        $nodeShipper->addChild(
            'CountryName',
            $this->getCountryParams($request->getShipperAddressCountryCode())->getName()
        );
        $nodeContact = $nodeShipper->addChild('Contact', '', '');
        $nodeContact->addChild('PersonName', substr($request->getShipperContactPersonFullName(), 0, 34));
        $nodeContact->addChild('PhoneNumber', substr($request->getShipperContactPhoneNumber(), 0, 24));

        $xml->addChild('LabelImageFormat', 'PDF', '');

        // If seller and Admin want to display custom logo.
        list($logoPath, $extension) = $this->_getLogoImage();
       
        if ($logoPath != '') {
            $logo = base64_encode(file_get_contents($logoPath));
            if (strlen($logo) > 1048576) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Logo image size too large.'));
            } else {
                $nodeLabel = $xml->addChild('Label', '', '');
                $nodeLabel->addChild('Logo', 'Y');
                $customerLogo = $nodeLabel->addChild('CustomerLogo', '', '');
                $customerLogo->addChild('LogoImage', $logo);
                $customerLogo->addChild('LogoImageFormat', strtoupper($extension));
                $nodeLabel->addChild('Resolution', '200');
            }
        }
        $requestData = $xml->asXML();
        if (!$requestData && !mb_detect_encoding($requestData) == 'UTF-8') {
            $requestData = utf8_encode($requestData);
        }
        $debugData = ['request' => $requestData];
        try {
            $client = $this->_httpClientFactory->create();
            $client->setUri((string) $this->_getGatewayUrl());
            $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
            $client->setRawData($requestData);
            $responseBody = $client->request(\Magento\Framework\HTTP\ZendClient::POST)->getBody();
            $responseBody = utf8_decode($responseBody);
            $debugData['result'] = $responseBody;
        } catch (\Exception $e) {
            $this->_errors[$e->getCode()] = $e->getMessage();
            $responseBody = '';
        }
        
        $this->_debug($debugData);

        return $responseBody;
    }
    /**
     * If admin allowed to display logo on label
     * @return array
     */
    protected function _getLogoImage()
    {
        $logoPath = '';
        $extension = 'JPG';
        $marketplaceHelper = $this->_objectManager->create('Webkul\Marketplace\Helper\Data');
        if ($this->getConfigData('dhl_logo_display')) {
            $logoPath = $marketplaceHelper->getMediaUrl().'dhl/logo/'.$this->getConfigData('dhl_logo');
            $extensionArray = explode('.', $logoPath);
            $extension = end($extensionArray);
        }

        if ($this->getConfigData('allow_seller_logo')) {
            $displaySellerLogo = $this->_customerSession->getCustomer()->getDhlLogo();
            if ($displaySellerLogo) {
                $seller = $marketplaceHelper->getSeller();
                $logoPath = $marketplaceHelper->getMediaUrl().'avatar/'.$seller['logo_pic'];
                $extensionArray = explode('.', $logoPath);
                $extension = end($extensionArray);
            } elseif ($this->getConfigData('dhl_logo_display')) {
                $logoPath = $marketplaceHelper->getMediaUrl().'dhl/logo/'.$this->getConfigData('dhl_logo');
                $extensionArray = explode('.', $logoPath);
                $extension = end($extensionArray);
            }
        }

        return [$logoPath, $extension];
    }
    /**
     * Add seller items details in request.
     * @param string                          $xml
     * @param  \Magento\Framework\DataObject $request
     * @param string                         $originRegionCode
     */
    public function _setItemsDetails($xml, $request, $originRegionCode)
    {
        $order = $this->_order;
        $customerId = $this->_customerSession->getCustomerId();
        $pieces = 0;
        foreach ($order->getAllItems() as $itemShipment) {
            if ($itemShipment->getProduct()->isVirtual() || $itemShipment->getParentItem()) {
                continue;
            }
            $collection = $this->_objectManager->create(
                'Webkul\Marketplace\Model\Product'
            )->getCollection()
            ->addFieldToFilter('mageproduct_id', ['eq' => $itemShipment->getProductId()])
            ->addFieldToFilter('seller_id', $customerId);

            if (count($collection) > 0) {
                ++$pieces;
            }
        }

        $nodeShipmentDetails = $xml->addChild('ShipmentDetails', '', '');
        $nodeShipmentDetails->addChild('NumberOfPieces', count($pieces));
        $nodePieces = $nodeShipmentDetails->addChild('Pieces', '', '');

        if ($originRegionCode) {
            $nodeShipmentDetails->addChild(
                'CurrencyCode',
                $this->_storeManager->getStore()->getBaseCurrencyCode()
            );
        }

        $i = 0;
        $weight = 0;
        $salesListModel = [];
        $itemsDesc = [];
        $itemsQty = 0;
        foreach ($order->getAllItems() as $itemShipment) {
            if ($itemShipment->getProduct()->isVirtual() || $itemShipment->getParentItem()) {
                continue;
            }
            $salesListModel = $this->_objectManager->create(
                'Webkul\Marketplace\Model\Saleslist'
            )
            ->getCollection()
            ->addFieldToFilter('seller_id', $customerId)
            ->addFieldToFilter('order_id', $order->getId())
            ->addFieldToFilter('mageproduct_id', $itemShipment->getProductId())
            ->addFieldToFilter('order_item_id', $itemShipment->getItemId());

            $unitweight = $itemShipment->getWeight() * $itemShipment->getQtyOrdered();

            if (count($salesListModel)) {
                if ($itemShipment->getHasChildren() && $itemShipment->isShipSeparately()) {
                    foreach ($itemShipment->getChildren() as $child) {
                        if (!$child->getProduct()->isVirtual()) {
                            $weight = $itemShipment->getWeight() * $itemShipment->getQty();
                        }
                    }
                } else {
                    $weight += $itemShipment->getWeight() * $itemShipment->getQtyOrdered();
                }
                $itemsQty += $itemShipment->getQtyOrdered();
                $itemsDesc = $itemShipment->getName();

                $nodePiece = $nodePieces->addChild('Piece', '', '');

                $packageType = 'EE';
                if ($this->getConfigData('content_type') == self::DHL_CONTENT_TYPE_NON_DOC) {
                    $packageType = 'CP';
                }
                $nodePiece->addChild('PieceID', ++$i);
                $nodePiece->addChild('PackageType', $packageType);
                $nodePiece->addChild('Weight', round($this->_getWeight($unitweight), 1));
                $nodePiece->addChild('PieceContents', substr($itemsDesc, 0, 34));
                if ($this->getConfigData('width') &&
                    $this->getConfigData('length') &&
                    $this->getConfigData('height')
                ) {
                    if (!$originRegionCode) {
                        $nodePiece->addChild('Width', round($this->getConfigData('width')));
                        $nodePiece->addChild('Height', round($this->getConfigData('height')));
                        $nodePiece->addChild('Depth', round($this->getConfigData('length')));
                    } else {
                        $nodePiece->addChild('Depth', round($this->getConfigData('length')));
                        $nodePiece->addChild('Width', round($this->getConfigData('width')));
                        $nodePiece->addChild('Height', round($this->getConfigData('height')));
                    }
                }
            } else {
                if ($itemShipment->getHasChildren() && $itemShipment->isShipSeparately()) {
                    foreach ($itemShipment->getChildren() as $child) {
                        if (!$child->getProduct()->isVirtual()) {
                            $weight = $itemShipment->getWeight() * $itemShipment->getQty();
                        }
                    }
                } else {
                    $weight += $itemShipment->getWeight() * $itemShipment->getQtyOrdered();
                }
                $defaultWeight = 0;
                if ($this->_getWeightUnit() == 'KG') {
                    $defaultWeight = round(0.226796, 3);
                } elseif ($this->_getWeightUnit() == 'LBS') {
                    $defaultWeight = 0.5;
                }
                if ($weight == 0) {
                    $weight = $defaultWeight;
                }
                $itemsQty += $itemShipment->getQtyOrdered();
                $itemsDesc = $itemShipment->getName();

                $nodePiece = $nodePieces->addChild('Piece', '', '');

                $packageType = 'EE';
                if ($this->getConfigData('content_type') == self::DHL_CONTENT_TYPE_NON_DOC) {
                    $packageType = 'CP';
                }
                $nodePiece->addChild('PieceID', ++$i);
                $nodePiece->addChild('PackageType', $packageType);
                $nodePiece->addChild('Weight', round($this->_getWeight($unitweight), 1));
                if ($this->getConfigData('width') &&
                    $this->getConfigData('depth') &&
                    $this->getConfigData('height')
                ) {
                    if (!$originRegionCode) {
                        $nodePiece->addChild('Width', round($this->getConfigData('width')));
                        $nodePiece->addChild('Height', round($this->getConfigData('height')));
                        $nodePiece->addChild('Depth', round($this->getConfigData('depth')));
                    } else {
                        $nodePiece->addChild('Depth', round($this->getConfigData('depth')));
                        $nodePiece->addChild('Width', round($this->getConfigData('width')));
                        $nodePiece->addChild('Height', round($this->getConfigData('height')));
                    }
                }
                $nodePiece->addChild('PieceContents', substr($itemsDesc, 0, 34));
            }
        }

        if (!$originRegionCode) {
            $nodeShipmentDetails->addChild('Weight', round($this->_getWeight($weight), 1));
            $nodeShipmentDetails->addChild('WeightUnit', substr($this->_getWeightUnit(), 0, 1));
            $nodeShipmentDetails->addChild('GlobalProductCode', $this->_getServiceCode());
            $nodeShipmentDetails->addChild('LocalProductCode', $this->_getServiceCode());
            $nodeShipmentDetails->addChild('Date', $this->_coreDate->date('Y-m-d'));
            $nodeShipmentDetails->addChild('Contents', 'DHL Parcel');
            /*
             * The DoorTo Element defines the type of delivery service that applies to the shipment.
             * The valid values are DD (Door to Door), DA (Door to Airport) , AA and DC (Door to
             * Door non-compliant)
             */
            $nodeShipmentDetails->addChild('DoorTo', 'DD');
            $nodeShipmentDetails->addChild('DimensionUnit', substr($this->_getDimensionUnit(), 0, 1));
            if ($this->getConfigData('content_type') == self::DHL_CONTENT_TYPE_NON_DOC) {
                $packageType = 'CP';
            }
            $nodeShipmentDetails->addChild('PackageType', $packageType);
            if ($this->isDutiable(
                $request->getShipperAddressCountryCode(),
                $request->getRecipientAddressCountryCode()
            )
            ) {
                $nodeShipmentDetails->addChild('IsDutiable', 'Y');
            }
            $nodeShipmentDetails->addChild(
                'CurrencyCode',
                $this->_storeManager->getStore()->getBaseCurrencyCode()
            );
        } else {
            if ($this->getConfigData('content_type') == self::DHL_CONTENT_TYPE_NON_DOC) {
                $packageType = 'CP';
            }
            $nodeShipmentDetails->addChild('PackageType', $packageType);
            $nodeShipmentDetails->addChild('Weight', $this->_getWeight($weight));
            $nodeShipmentDetails->addChild('DimensionUnit', substr($this->_getDimensionUnit(), 0, 1));
            $nodeShipmentDetails->addChild('WeightUnit', substr($this->_getWeightUnit(), 0, 1));
            $nodeShipmentDetails->addChild('GlobalProductCode', $this->_getServiceCode());
            $nodeShipmentDetails->addChild('LocalProductCode', $this->_getServiceCode());

            /*
             * The DoorTo Element defines the type of delivery service that applies to the shipment.
             * The valid values are DD (Door to Door), DA (Door to Airport) , AA and DC (Door to
             * Door non-compliant)
             */
            $nodeShipmentDetails->addChild('DoorTo', 'DD');
            $nodeShipmentDetails->addChild('Date', $this->_coreDate->date('Y-m-d'));
            $nodeShipmentDetails->addChild('Contents', 'DHL Parcel TEST');
        }
    }
    /**
     * set recipent and seller data in object.
     */
    public function setShipemntRequest()
    {
        $request = $this->_rawRequest;
        $r = new \Magento\Framework\DataObject();
        $order = $this->_order;
        $customerId = $this->_customerSession->getCustomerId();

        //set default credentials
        $r->setAccessId($this->getConfigData('id'));
        $r->setPassword($this->getConfigData('password'));
        $r->setAccountNumber($this->getConfigData('account'));
        $r->setReadyTime($this->getConfigData('ready_time'));

        //set seller credentials
        $this->_isSellerHasOwnCredentials($r, $customerId);

        $destAddress = $order->getShippingAddress();

        //set Recipient Address
        $street = $destAddress->getStreet();
        $r->setRecipientAddressStreet1($street[0]);
        if (count($street) < 2) {
            $r->setRecipientAddressStreet2('');
        } else {
            $r->setRecipientAddressStreet2($street[1]);
        }
        $r->setRecipientContactPersonFullName($destAddress->getFirstname().' '.$destAddress->getLastname());
        $r->setRecipientAddressPostalCode($destAddress->getPostcode());
        $r->setRecipientAddressCity($destAddress->getCity());
        $region = $this->_region->create()->load($destAddress->getRegionId())->getCode();
        if ($region != '') {
            $r->setRecipientAddressStateOrProvinceCode($region);
        } else {
            $r->setRecipientAddressStateOrProvinceCode($destAddress->getCountryId());
        }
        $r->setRecipientAddressCountryCode($destAddress->getCountryId());
        $r->setRecipientContactPhoneNumber($destAddress->getTelephone());
        if ($destAddress->getCompany() != '') {
            $r->setRecipientContactCompanyName($destAddress->getCompany());
        } else {
            $r->setRecipientContactCompanyName($destAddress->getFirstname());
        }
        //Set Sender Address.


        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $address = $customer->getDefaultShipping();
        if ($address) {
            $addressModel = $this->_objectManager->create('Magento\Customer\Model\Address')->load($address);
            $add = $addressModel->getStreet();
            $r->setShipperAddressStreet1($add[0]);
            if (count($add) < 2) {
                $r->setShipperAddressStreet2('');
            } else {
                $r->setShipperAddressStreet2($add[1]);
            }
            $r->setShipperContactPersonFullName($customer->getFirstname().' '.$customer->getLastname());
            $r->setShipperAddressPostalCode($addressModel->getPostcode());
            $r->setShipperAddressCountryCode($addressModel->getCountryId());
            $r->setShipperAddressCity($addressModel->getCity());
            $r->setShipperContactPhoneNumber($addressModel->getTelephone());
            $company = $addressModel->getCompany() != '' ? $addressModel->getCompany() : $customer->getFirstname();
            $r->setShipperContactCompanyName($company);
            $reginCode = $this->_region->create()->load($addressModel->getRegionId())->getCode();
            $reginCode = $reginCode != '' ? $reginCode : $addressModel->getCountryId();
            $r->setShipperAddressStateOrProvinceCode($reginCode);

            $r->setShipperAddressError(false);
        } else {
            $originStreet2 = $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ADDRESS2,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            );
            $r->setShipperAddressStreet1($request->getShipperAddressStreet1());
            $r->setShipperAddressStreet2($originStreet2);
            $r->setShipperContactPersonFullName($request->getShipperContactPersonName());
            $r->setShipperAddressPostalCode($request->getShipperAddressPostalCode());
            $r->setShipperAddressCountryCode($request->getShipperAddressCountryCode());
            $r->setShipperAddressCity($request->getShipperAddressCity());
            $r->setShipperContactPhoneNumber($request->getShipperContactPhoneNumber());
            $r->setShipperContactCompanyName($request->getShipperContactCompanyName());
            $r->setShipperAddressStateOrProvinceCode($request->getShipperAddressStateOrProvinceCode());
        }
        $totalPrice = 0;
        foreach ($order->getAllItems() as $itemShipment) {
            if ($itemShipment->getProduct()->isVirtual() || $itemShipment->getParentItem()) {
                continue;
            }
            $totalPrice += $itemShipment->getRowTotalInclTax();
        }
        $totalPrice += $order->getShippingAmount();
        $r->setTotalValue($totalPrice);
        $request->setShipmentDetails($r);
        $this->setRawRequest($request);

        return $this;
    }
    /**
     * Is MpDHLShipping Method.
     * @return boolean
     */
    protected function _isShippingMethod()
    {
        $shippingmethod = $this->_order->getShippingMethod();
        if (strpos($shippingmethod, 'mpdhl') !== false) {
            return true;
        }

        return false;
    }
    /**
     * return service type for shipment.
     *
     * @return string
     */
    protected function _getServiceCode()
    {
        $shippingmethod = explode('mpdhl_', $this->_order->getShippingMethod());

        return $shippingmethod[1];
    }

    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        return true;
    }
        /**
         * get assign product id
         * @param  object $item
         * @return int
         */
    protected function _getAssignProduct($item)
    {
        $mpassignproductId = 0;
        $itemOption = $this->_objectManager
            ->create('Magento\Quote\Model\Quote\Item\Option')
            ->getCollection()
            ->addFieldToFilter('item_id', ['eq' => $item->getId()])
            ->addFieldToFilter('code', ['eq' => 'info_buyRequest']);
        $optionValue = '';
        if (count($itemOption)) {
            foreach ($itemOption as $value) {
                $optionValue = $value->getValue();
            }
        }
        if ($optionValue != '') {
            $temp = json_encode($optionValue, true);
            $mpassignproductId = isset($temp['mpassignproduct_id']) ? $temp['mpassignproduct_id'] : 0;
        }
        return $mpassignproductId;
    }
    /**
     * get seller id
     * @param  int $mpassignproductId
     * @param  int $proid
     * @return int
     */
    protected function _getSellerId($mpassignproductId, $proid)
    {
        $sellerId = 0;
        if ($mpassignproductId) {
            $mpassignModel = $this->_loadModel($mpassignproductId, 'Webkul\MpAssignProduct\Model\Items');
            $sellerId = $mpassignModel->getSellerId();
        } else {
            $collection = $this->_objectManager->create('Webkul\Marketplace\Model\Product')
                                ->getCollection()
                                ->addFieldToFilter('mageproduct_id', ['eq' => $proid]);
            foreach ($collection as $temp) {
                $sellerId = $temp->getSellerId();
            }
        }

        return $sellerId;
    }
    /**
     * get product weight
     * @param  object $item
     * @return int
     */
    protected function _getItemWeight($item)
    {
        $weight = 0;
        if ($item->getHasChildren() && $item->isShipSeparately()) {
            foreach ($item->getChildren() as $child) {
                if (!$child->getProduct()->isVirtual()) {
                    $weight = $item->getWeight() * $item->getQty();
                }
            }
        } else {
            $weight = $item->getWeight() * $item->getQty();
        }

        return $weight;
    }

    /**
     * Convert item weight to needed weight based on config weight unit dimensions
     *
     * @param float $weight
     * @param bool $maxWeight
     * @param string|bool $configWeightUnit
     * @return float
     */
    protected function _getActualWeight($weight)
    {
        $configWeightUnit = $this->getCode(
            'dimensions_variables',
            (string)$this->getConfigData('unit_of_measure')
        );
        $countryWeightUnit = $this->getCode('dimensions_variables', $this->_getItemWeightUnit());

        if ($configWeightUnit != $countryWeightUnit) {
            $weight = $this->_carrierHelper->convertMeasureWeight(
                round($weight, 3),
                $configWeightUnit,
                $countryWeightUnit
            );
        }
        
        return round($weight, 3);
    }

    /**
     * Returns weight unit (kg or pound)
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getItemWeightUnit()
    {
        $countryId = $this->originCountryId;
        $weightUnit = $this->getCountryParams($countryId)->getWeightUnit();
        if (empty($weightUnit)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("Cannot identify weight unit for %1", $countryId)
            );
        }

        return $weightUnit;
    }

    /**
     * load model
     * @param  int $id
     * @param  string $model
     * @return object
     */
    protected function _loadModel($id, $model)
    {
        return $this->_objectManager->create($model)->load($id);
    }
}
